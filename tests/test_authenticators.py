import pytest
from pyramid import testing


class TestUserModelAuthenticator(object):

    @pytest.fixture
    def target(self):
        from rebecca.users.authenticators import UserModelAuthenticator
        return UserModelAuthenticator

    def test_authenticate(self, target):
        request = testing.DummyRequest()
        context = testing.DummyResource()
        user = testing.DummyResource(
            password_digest=b'dummy:secret:',
        )
        secret = ":secret:"
        user_model = testing.DummyResource(
            name = testing.DummyResource(),
            query=testing.DummyResource(
                filter=lambda *args: testing.DummyResource(first=lambda: user),
            ),
        )
        authenticator = target(context, user_model, secret, request)
        authenticator.digest_alg = lambda password: testing.DummyResource(
            hexdigest=lambda: password,
        )
        result = authenticator.authenticate('dummy', 'dummy')
        assert result == user

    def test_authenticate_invalid_password(self, target):
        request = testing.DummyRequest()
        context = testing.DummyResource()
        user = testing.DummyResource(
            password_digest=b'dummy',
        )
        secret = ":secret:"
        user_model = testing.DummyResource(
            name = testing.DummyResource(),
            query=testing.DummyResource(
                filter=lambda *args: testing.DummyResource(first=lambda: user),
            ),
        )
        authenticator = target(context, user_model, secret, request)
        authenticator.digest_alg = lambda password: testing.DummyResource(
            hexdigest=lambda: password,
        )
        result = authenticator.authenticate('dummy', 'dummy')
        assert result is None

    def test_authenticate_invalid_user(self, target):
        request = testing.DummyRequest()
        context = testing.DummyResource()
        user_model = testing.DummyResource(
            name = testing.DummyResource(),
            query=testing.DummyResource(
                filter=lambda *args: testing.DummyResource(first=lambda: None),
            ),
        )
        authenticator = target(context, user_model, None, request)
        result = authenticator.authenticate('dummy', 'dummy')
        assert result is None
