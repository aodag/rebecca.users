import pytest
from pyramid import testing
import pyramid.request


settings = {
    "rebecca.users.authenticator.secret": "",
}


@pytest.fixture
def config():
    config = testing.setUp(settings=settings)
    yield config
    testing.tearDown()


@pytest.fixture
def models(sql_session, custom_user_model):
    from rebecca.users import models
    models.BaseObject.metadata.create_all(bind=sql_session.bind)
    return models


@pytest.fixture(scope="session")
def custom_user_model():
    from sqlalchemy import Column, Unicode
    from rebecca.users import models

    class CustomUser(models.User):
        info = Column(Unicode(255))
        __mapper_args__ = {
            'polymorphic_identity':'custom_user',
        }

    return CustomUser


def test_it(sql_session, config, models):
    from rebecca.users.authenticators import UserModelAuthenticator

    config.include("rebecca.security")
    config.include("rebecca.users")

    request = testing.DummyRequest()
    context = testing.DummyResource()
    pyramid.request.apply_request_extensions(request)

    p = UserModelAuthenticator.digest_alg(b"password").hexdigest()
    user = models.User(name=u"dummy", password_digest=p)
    sql_session.add(user)

    result = request.authenticate(u"dummy", u"password")
    assert result == user


def test_custom_user(sql_session, config, custom_user_model):
    from rebecca.users.authenticators import UserModelAuthenticator

    config.include("rebecca.security")
    config.registry.settings.update({
        "rebecca.users.user_model": custom_user_model,
    })
    config.include("rebecca.users")

    request = testing.DummyRequest()
    context = testing.DummyResource()
    pyramid.request.apply_request_extensions(request)

    p = UserModelAuthenticator.digest_alg(b"password").hexdigest()
    user = custom_user_model(name=u"dummy", password_digest=p)
    sql_session.add(user)

    result = request.authenticate(u"dummy", u"password")
    assert result == user


def test_introspectable(sql_session, config, models):
    from rebecca.users.authenticators import UserModelAuthenticator

    config.include("rebecca.security")
    config.include("rebecca.users")
    introspector = config.registry.introspector
    intr = introspector.get('rebecca', 'rebecca.security.authenticator')
    assert intr
    assert intr.title == "authenticator"
    assert intr.type_name == "rebecca.users.authenticators.UserModelAuthenticatorFactory"
    assert intr["user_model"] == 'rebecca.users.models.User'
