import pytest


class TestUser(object):

    @pytest.fixture
    def target(self):
        from rebecca.users.models import User
        return User

    def test_init(self, target):
        user = target()
        assert user.roles == []


class TestRole(object):

    @pytest.fixture
    def target(self):
        from rebecca.users.models import Role
        return Role

    def test_init(self, target):
        role = target()
        assert role.users == []
        assert role.permissions == []


class TestPermission(object):

    @pytest.fixture
    def target(self):
        from rebecca.users.models import Permission
        return Permission

    def test_init(self, target):
        permission = target()
        assert permission.roles == []
