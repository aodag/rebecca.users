Getting Started
===========================

::

   $ pip install -e .[testing,dev] -c constraints.txt
   $ py.test --cov=rebecca.users --cov-report=term-missing
   $ flake8 rebecca
