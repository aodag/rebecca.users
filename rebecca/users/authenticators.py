import hashlib
from zope.interface import implementer
from .interfaces import IAuthenticator


@implementer(IAuthenticator)
class UserModelAuthenticator(object):
    digest_alg = hashlib.sha256

    def __init__(self, context, user_model, secret, request):
        self.context = context
        self.user_model = user_model
        self.secret = secret
        self.request = request

    def authenticate(self, username, password):
        user = self.user_model.query.filter(
            self.user_model.name == username,
        ).first()

        if user is None:
            return None

        if user.password_digest != self.digest_password(password):
            return None

        return user

    def digest_password(self, password):
        data = (password + self.secret).encode('utf-8')
        return self.digest_alg(data).hexdigest()


class UserModelAuthenticatorFactory(object):

    __name__ = 'UserModelAuthenticatorFactory'

    def __init__(self, user_model, secret):
        self.user_model = user_model
        self.secret = secret

    def __call__(self, context, request):
        return UserModelAuthenticator(
            context, self.user_model, self.secret, request)
