from pyramid.interfaces import IRequest
from zope.interface import Interface
from .authenticators import UserModelAuthenticatorFactory


def includeme(config):
    reg = config.registry
    user_model_name = reg.settings.get(
        "rebecca.users.user_model", "rebecca.users.models.User")
    user_model = config.maybe_dotted(user_model_name)
    secret = config.registry.settings["rebecca.users.authenticator.secret"]
    factory = UserModelAuthenticatorFactory(user_model, secret)
    config.add_authenticator(factory, {"user_model": user_model.__module__ + "." + user_model.__name__})
