from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    String,
    Table,
    Unicode,
)
from sqlalchemy.orm import relationship
from pyramid_sqlalchemy import (
    BaseObject,
    Session,
)

users_roles = Table(
    "rebecca_users_roles",
    BaseObject.metadata,
    Column(
        "user_id",
        Integer,
        ForeignKey(
            "rebecca_users.id",
            name="fk_rebecca_roles_users",
        ),
    ),
    Column(
        "role_id",
        Integer,
        ForeignKey(
            "rebecca_roles.id",
            name="fk_rebecca_users_roles",
        ),
    ),
)

roles_permissions = Table(
    "rebecca_roles_permissions",
    BaseObject.metadata,
    Column(
        "role_id",
        Integer,
        ForeignKey(
            "rebecca_roles.id",
            name="fk_rebecca_permissions_roles",
        ),
    ),
    Column(
        "permission_id",
        Integer,
        ForeignKey(
            "rebecca_permissions.id",
            name="fk_rebecca_roles_permissions",
        ),
    ),
)


class User(BaseObject):
    """
    """
    __tablename__ = 'rebecca_users'
    query = Session.query_property()
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    password_digest = Column(String(255))
    type = Column(Unicode(255))
    roles = relationship(
        "Role", secondary=users_roles, backref="users")
    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 'rebecca_user',
    }


class Role(BaseObject):
    """
    """
    __tablename__ = 'rebecca_roles'
    query = Session.query_property()
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    permissions = relationship(
        "Permission", secondary=roles_permissions, backref="roles")


class Permission(BaseObject):
    """
    """
    __tablename__ = 'rebecca_permissions'
    query = Session.query_property()
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
